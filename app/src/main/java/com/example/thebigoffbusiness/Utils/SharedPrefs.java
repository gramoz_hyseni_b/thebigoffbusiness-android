package com.example.thebigoffbusiness.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by dell on 3/25/2018.
 */

public class SharedPrefs {
    private static final String PROVIDER ="PROVIDER" ;
    private static final String USER_ID="USER_ID";
    private static final String USER_TOKEN="USER_TOKEN";
    private static final String LOGED_IN="LOGED_IN";
    private static final String USER_SESION="USER_SESION";
    private static final String USER_NUMBEROFPRODUCTS = "USER_NUMBEROFPRODUCTS";
    private static final String USER_ADDRESS = "USER_ADDRESS";
    private static final String USER_PHONE_NUMBER = "USER_PHONE_NUMBER";
    private static final String USER_REFRESHED_TOKEN_ID = "USER_REFRESHED_TOKEN_ID";
    private static final String ORDER_ID = "ORDER_ID";
    private static final String LANGUAGE = "LANGUAGE";

//    private static final String URL_SWITCH = "URL_SWITCH";

    private Context mContext;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    public static SharedPrefs sharedPrefs;

    private SharedPrefs(Context context){
        this.mContext = context;
        sharedPreferences = mContext.getSharedPreferences(USER_SESION, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static SharedPrefs getSharedPrefs(Context context){
        if(sharedPrefs == null){
            sharedPrefs = new SharedPrefs(context);
        }
        return sharedPrefs;
    }

    public void saveUserRefreshedTokenID(String userRefreshedTokenID){
        editor.putString(USER_REFRESHED_TOKEN_ID, userRefreshedTokenID);
        editor.apply();
    }

    public void saveLanguage(String language){
        editor.putString(LANGUAGE, language);
        editor.apply();
    }

    public void saveOrderID(String orderID) {
        editor.putString(ORDER_ID, orderID);
        editor.apply();
    }

    //save USER_ID & USER_TOKEN in Shared Preferences
    public void saveUserTokenAndID(String userID, String userToken, String provider){
        editor.putString(USER_ID,userID);
        editor.putString(USER_TOKEN,userToken);
        editor.putString(PROVIDER,provider);
        editor.apply();
    }

    //save NumberOfProductsInCart
    public void saveUserNumberOfProducts(String number){
        editor.putString(USER_NUMBEROFPRODUCTS,number);
        editor.apply();
    }

    public void saveUserAddress(String address){
        editor.putString(USER_ADDRESS,address);
        editor.apply();
    }

    public void saveUserPhoneNumber(String phoneNumber){
        editor.putString(USER_PHONE_NUMBER,phoneNumber);
        editor.apply();
    }

//    public void saveUrl(String url){
//        editor.putString(URL_SWITCH,url);
//        editor.apply();
//    }
//
//    public String getUrl() {
//        String userRefreshedTokenID = sharedPreferences.getString(URL_SWITCH, null);
//        return userRefreshedTokenID;
//    }

    public String getLanguage(){
        String language = sharedPreferences.getString(LANGUAGE, null);
        return language;
    }

    public String getOrderID() {
        String orderID = sharedPreferences.getString(ORDER_ID, null);
        return orderID;
    }

    public String getUserRefreshedTokenID() {
        String userRefreshedTokenID = sharedPreferences.getString(USER_REFRESHED_TOKEN_ID, null);
        return userRefreshedTokenID;
    }

    public String getUserAddress() {
        String userAdress = sharedPreferences.getString(USER_ADDRESS, null);
        return userAdress;
    }

    public String getUserPhoneNumber() {
        String userPhoneNumber = sharedPreferences.getString(USER_PHONE_NUMBER, null);
        return userPhoneNumber;
    }

    //get NumberOfProductsInCart
    public String getUserNumberofproducts(){
        String userNumberofproducts = sharedPreferences.getString(USER_NUMBEROFPRODUCTS, null);
        return userNumberofproducts;
    }

    public String getUserId(){
        String userId = sharedPreferences.getString(USER_ID, null);
        return userId;
    }

    public String getUserToken(){
        String userToken = sharedPreferences.getString(USER_TOKEN, null);
        return userToken;
    }

    //check user session
    public boolean logedIn(){
        //check if user is loged in and has selected at least three preferences
        return sharedPreferences.getBoolean(LOGED_IN,false);
    }

    //delete USER_DETAILS when user log out
    public void logout(){
        startSession(false);
        editor.clear();
        editor.apply();
    }
    // create user session
    public void startSession(boolean logedIn){
        String mProvider = sharedPreferences.getString(PROVIDER,null);
        editor.putString(PROVIDER,mProvider);
        editor.putBoolean(LOGED_IN,logedIn);
        editor.apply();
    }
}
