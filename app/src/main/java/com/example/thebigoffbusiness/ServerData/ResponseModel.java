package com.example.thebigoffbusiness.ServerData;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dell on 3/25/2018.
 */

public class ResponseModel
{
    @SerializedName(".issued")
    private String issued;

    @SerializedName(".expires")
    private String expires;

    @SerializedName("ID")
    private String ID;

    private String userName;

    private String expires_in;

    private String token_type;
    @SerializedName("TheCountOfTheCategories")
    private String preferencesCount;

    private String access_token;

    public void setissued (String issued)
    {
        this.issued = issued;
    }

    public String getexpires ()
    {
        return expires;
    }

    public String getID ()
    {
        return ID;
    }

    public String getPreferencesCount(){
        return this.preferencesCount;
    }

    public String getAccess_token ()
    {
        return access_token;
    }

    @Override
    public String toString() {
        return "ResponseModel{" +
                "ID='" + ID + '\'' +
                ", userName='" + userName + '\'' +
                ", access_token='" + access_token.substring(0,10) + '\'' +
                '}';
    }
}