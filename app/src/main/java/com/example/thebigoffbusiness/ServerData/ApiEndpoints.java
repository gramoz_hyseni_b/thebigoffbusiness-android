package com.example.thebigoffbusiness.ServerData;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by dell on 1/31/2018.
 */

public interface ApiEndpoints {

    //Login Endpoint
    @POST("Token")
    @FormUrlEncoded
    Call<ResponseModel> login(@Field("username") String username,
                              @Field("password") String password,
                              @Field("grant_type") String grant_type);

}
