package com.example.thebigoffbusiness.ServerData;

import android.content.Context;
import android.content.Intent;

import com.example.thebigoffbusiness.Utils.SharedPrefs;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dell on 1/31/2018.
 */

public class ApiClient {

    private static Context context = null;
    private static SharedPrefs sharedPrefs;

    public static Retrofit apiClient;
    //public static String BASE_URL="https://api.thebigoff.com/";
    public static String BASE_URL="https://bapi.thebigoff.com/";

//    public final static OkHttpClient okHttpClient = new OkHttpClient.Builder()
//            .connectTimeout(1, TimeUnit.MINUTES)
//            .writeTimeout(1, TimeUnit.MINUTES)
//            .readTimeout(1, TimeUnit.MINUTES)
//            .build();

    public final static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
            .addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request request = chain.request();
                    okhttp3.Response response = chain.proceed(request);

                    if (response.code() == 401) {

                        sharedPrefs.startSession(false);

                        return response;
                    }

                    return response;
                }
            })
            .build();

    public static Retrofit getApiClient(Context c)
    {
        context = c;
        sharedPrefs = SharedPrefs.getSharedPrefs(context);

        if(apiClient==null) {
            apiClient=new Retrofit.Builder().client(okHttpClient).baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return apiClient;
    }

    public static Retrofit getApiClient()
    {
        if(apiClient==null) {
            apiClient=new Retrofit.Builder().client(okHttpClient).baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return apiClient;
    }
}
