package com.example.thebigoffbusiness.Login;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.thebigoffbusiness.Home.HomeActivity;
import com.example.thebigoffbusiness.R;

public class LoginActivity extends AppCompatActivity {

    private EditText username,password;
    private TextView forgot_password;
    private Button loginbuttton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        username = findViewById(R.id.username);
        password = findViewById(R.id.passwordtxt);
        forgot_password = findViewById(R.id.forgot_password);
        loginbuttton = findViewById(R.id.loginbuttton);

        loginbuttton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
    }
}
