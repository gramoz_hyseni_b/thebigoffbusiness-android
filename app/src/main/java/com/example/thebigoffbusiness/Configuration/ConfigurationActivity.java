package com.example.thebigoffbusiness.Configuration;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.thebigoffbusiness.ChangePassword.ChangePasswordActivity;
import com.example.thebigoffbusiness.Login.LoginActivity;
import com.example.thebigoffbusiness.R;

public class ConfigurationActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView change_password,log_out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        change_password = findViewById(R.id.change_password);
        log_out = findViewById(R.id.log_out);

        change_password.setOnClickListener(this);
        log_out.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.change_password:
                startActivity(new Intent(getApplicationContext(), ChangePasswordActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
            case R.id.log_out:
                startActivity(new Intent(getApplicationContext(), LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
                default:
                    break;
        }
    }
}
