package com.example.thebigoffbusiness.Home.Fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.thebigoffbusiness.Home.Adapters.AllProductsAdapter;
import com.example.thebigoffbusiness.Home.Model.SelectedDayProductsModel;
import com.example.thebigoffbusiness.Home.Model.SingleProductsModel;
import com.example.thebigoffbusiness.R;

import java.util.ArrayList;
import java.util.List;

public class AllProductsFragment extends Fragment {


    private RecyclerView recyclerView;
    private AllProductsAdapter allProductsAdapter;
    private LinearLayoutManager linearLayoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View roortView = inflater.inflate(R.layout.all_products_fragment, container, false);

        recyclerView = roortView.findViewById(R.id.recyclerview);

        return roortView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        allProductsAdapter = new AllProductsAdapter(getContext(), init());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(allProductsAdapter);
    }

    private List<SelectedDayProductsModel> init(){
        List<SelectedDayProductsModel> selectedDayProductsModelList = new ArrayList<>();

        SingleProductsModel singleProductsModel = new SingleProductsModel("Nike Air Max", "#FF9F1A", "XL", "https://thebigoff.com/Content/products/productImage1942422180.jpg", 1, "Egzon", true, 321);
        List<SingleProductsModel> singleProductsModels = new ArrayList<SingleProductsModel>();
        singleProductsModels.add(singleProductsModel);

        SelectedDayProductsModel selectedDayProductsModel = new SelectedDayProductsModel("Sot",singleProductsModels,48.99);

        List<SingleProductsModel> singleProductsModels2 = new ArrayList<SingleProductsModel>();
        singleProductsModels2.add(singleProductsModel);
        singleProductsModels2.add(singleProductsModel);
        SelectedDayProductsModel selectedDayProductsModel2 = new SelectedDayProductsModel("Dje",singleProductsModels2,48.99);

        List<SingleProductsModel> singleProductsModels3 = new ArrayList<SingleProductsModel>();
        singleProductsModels3.add(singleProductsModel);
        singleProductsModels3.add(singleProductsModel);
        singleProductsModels3.add(singleProductsModel);
        SelectedDayProductsModel selectedDayProductsModel3 = new SelectedDayProductsModel("20.10.2019",singleProductsModels3,48.99);

        selectedDayProductsModelList.add(selectedDayProductsModel);
        selectedDayProductsModelList.add(selectedDayProductsModel2);
        selectedDayProductsModelList.add(selectedDayProductsModel3);

        return  selectedDayProductsModelList;
    }
}
