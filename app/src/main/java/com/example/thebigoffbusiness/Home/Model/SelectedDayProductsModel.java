package com.example.thebigoffbusiness.Home.Model;

import java.util.List;

public class SelectedDayProductsModel {
    private String Day;
    private List<SingleProductsModel> singleProductsModelList;
    private double Total;

    public SelectedDayProductsModel(String day, List<SingleProductsModel> singleProductsModelList, double total) {
        Day = day;
        this.singleProductsModelList = singleProductsModelList;
        Total = total;
    }

    public String getDay() {
        return Day;
    }

    public void setDay(String day) {
        Day = day;
    }

    public List<SingleProductsModel> getSingleProductsModelList() {
        return singleProductsModelList;
    }

    public void setSingleProductsModelList(List<SingleProductsModel> singleProductsModelList) {
        this.singleProductsModelList = singleProductsModelList;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(double total) {
        Total = total;
    }
}
