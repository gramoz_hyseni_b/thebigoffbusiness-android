package com.example.thebigoffbusiness.Home.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.thebigoffbusiness.Home.Model.SingleProductsModel;
import com.example.thebigoffbusiness.R;

import java.text.DecimalFormat;
import java.util.List;

public class SingleProductsAdapter extends RecyclerView.Adapter<SingleProductsAdapter.ViewHolder> {

    private Context context;
    private List<SingleProductsModel> singleProductsModelList;

    public SingleProductsAdapter(Context context, List<SingleProductsModel> singleProductsModelList) {
        this.context = context;
        this.singleProductsModelList = singleProductsModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.all_products_single_itemn, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final SingleProductsModel singleProductsModel = singleProductsModelList.get(i);

        try {
            Glide.with(context).load(singleProductsModel.getImage()).into(viewHolder.image);

            final GradientDrawable gradientDrawable = (GradientDrawable) viewHolder.product_basket_cicle_shape.getBackground();

            if(isColorWhite(singleProductsModel)){
                viewHolder.product_basket_cicle_shape.setBackground(context.getResources().getDrawable(R.drawable.imgcolorborder));
            }else
                gradientDrawable.setColor(Color.parseColor(singleProductsModel.getColor()));

            if(singleProductsModel.isUsedPromotionalCode())
                viewHolder.promotional_code_used.setVisibility(View.VISIBLE);
            else
                viewHolder.promotional_code_used.setVisibility(View.GONE);

            viewHolder.cmimi.setText(new DecimalFormat("##.##").format(singleProductsModel.getPrice()) + " €");

            viewHolder.madhesia.setText(singleProductsModel.getSize());
            viewHolder.sasia.setText("x" + singleProductsModel.getQuantity());
            viewHolder.klienti.setText(singleProductsModel.getClient());
        }catch (Exception e){

        }
    }

    @Override
    public int getItemCount() {
        return singleProductsModelList.size();
    }

    private boolean isColorWhite(SingleProductsModel mostSelledModel) {
        String[] arr = new String[] {"#FFFFFF", "#FFFFF", "#FFFF", "#FFF", "#FF","#F", "#ffffff","#fffff","#ffff","#fff","#ff","#f"};

        for(int i = 0 ; i < arr.length ; i++){
            if(mostSelledModel.getColor().equals(arr[i]))
                return true;
        }

        return false;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image,product_basket_cicle_shape;
        TextView madhesia,sasia,klienti,cmimi,promotional_code_used;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            product_basket_cicle_shape = itemView.findViewById(R.id.product_basket_cicle_shape);
            madhesia = itemView.findViewById(R.id.madhesia);
            sasia = itemView.findViewById(R.id.sasia);
            klienti = itemView.findViewById(R.id.klienti);
            cmimi = itemView.findViewById(R.id.cmimi);
            promotional_code_used = itemView.findViewById(R.id.promotional_code_used);
        }
    }
}
