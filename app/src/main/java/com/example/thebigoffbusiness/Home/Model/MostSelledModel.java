package com.example.thebigoffbusiness.Home.Model;

public class MostSelledModel {
    private String Name;
    private String Color;
    private String Size;
    private String Image;
    private int Quantity;
    private String Client;
    private String Shipper;
    private double Price;
    private int Sales;

    public MostSelledModel(String name, String color, String size, String image, int quantity, String client, String shipper, double price, int sales) {
        Name = name;
        Color = color;
        Size = size;
        Image = image;
        Quantity = quantity;
        Client = client;
        Shipper = shipper;
        Price = price;
        Sales = sales;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public String getClient() {
        return Client;
    }

    public void setClient(String client) {
        Client = client;
    }

    public String getShipper() {
        return Shipper;
    }

    public void setShipper(String shipper) {
        Shipper = shipper;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public int getSales() {
        return Sales;
    }

    public void setSales(int sales) {
        Sales = sales;
    }
}
