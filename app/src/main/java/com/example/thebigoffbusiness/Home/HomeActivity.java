package com.example.thebigoffbusiness.Home;

import android.content.Intent;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.thebigoffbusiness.Configuration.ConfigurationActivity;
import com.example.thebigoffbusiness.Home.Adapters.TabViewPagerAdapter;
import com.example.thebigoffbusiness.Home.Fragments.AllProductsFragment;
import com.example.thebigoffbusiness.Home.Fragments.BestSalingFragment;
import com.example.thebigoffbusiness.Notification.NotificationActivity;
import com.example.thebigoffbusiness.R;
import com.google.android.material.tabs.TabLayout;

public class HomeActivity extends AppCompatActivity {

    private RelativeLayout notification_holder;
    private TextView notification_count,business_name_text;
    private ImageView profile_image;
    private ViewPager viewpager;
    private TabLayout tabs;
    private TabViewPagerAdapter pagerAdapter;

    private AllProductsFragment allProductsFragment;
    private BestSalingFragment bestSalingFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        notification_holder = findViewById(R.id.notification_holder);
        notification_count = findViewById(R.id.notification_count);
        business_name_text = findViewById(R.id.business_name_text);
        profile_image = findViewById(R.id.profile_image);
        viewpager = findViewById(R.id.viewpager);
        tabs = findViewById(R.id.tabs);

        pagerAdapter = new TabViewPagerAdapter(getSupportFragmentManager());

        allProductsFragment = new AllProductsFragment();
        bestSalingFragment = new BestSalingFragment();

        pagerAdapter.addFragment(allProductsFragment, "Të gjitha");
        pagerAdapter.addFragment(bestSalingFragment, "Më të shiturat");

        tabs.setupWithViewPager(viewpager);
        viewpager.setAdapter(pagerAdapter);

        notification_holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), NotificationActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ConfigurationActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
    }
}
