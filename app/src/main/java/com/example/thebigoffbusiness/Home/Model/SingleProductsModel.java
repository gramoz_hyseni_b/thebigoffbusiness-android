package com.example.thebigoffbusiness.Home.Model;

public class SingleProductsModel {
    private String Name;
    private String Color;
    private String Size;
    private String Image;
    private int Quantity;
    private String Client;
    private boolean UsedPromotionalCode;
    private double Price;

    public SingleProductsModel(String name, String color, String size, String image, int quantity, String client, boolean usedPromotionalCode, double price) {
        Name = name;
        Color = color;
        Size = size;
        Image = image;
        Quantity = quantity;
        Client = client;
        UsedPromotionalCode = usedPromotionalCode;
        Price = price;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public String getClient() {
        return Client;
    }

    public void setClient(String client) {
        Client = client;
    }

    public boolean isUsedPromotionalCode() {
        return UsedPromotionalCode;
    }

    public void setUsedPromotionalCode(boolean usedPromotionalCode) {
        UsedPromotionalCode = usedPromotionalCode;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }
}
