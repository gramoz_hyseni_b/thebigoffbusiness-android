package com.example.thebigoffbusiness.Home.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.thebigoffbusiness.Home.Model.MostSelledModel;
import com.example.thebigoffbusiness.R;

import java.text.DecimalFormat;
import java.util.List;

public class MostSelledProductsAdapter extends RecyclerView.Adapter<MostSelledProductsAdapter.ViewHolder> {
    private Context context;
    private List<MostSelledModel> mostSelledModels;

    public MostSelledProductsAdapter(Context context, List<MostSelledModel> mostSelledModels) {
        this.context = context;
        this.mostSelledModels = mostSelledModels;

        mostSelledModels.add(new MostSelledModel("Nike Air Max",
                "#7A6460",
                "XL",
                "https://thebigoff.com/Content/products/productImage1942422180.jpg",
                1,
                "Egzon",
                "Posta e shpejtë",
                35.49,
                33));

        mostSelledModels.add(new MostSelledModel("Iphone",
                "#ffffff",
                "XXL",
                "https://thebigoff.com/Content/products/productImage1942422180.jpg",
                1,
                "Gramoz",
                "Posta e shpejtë",
                845.49,
                53));
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.most_selled_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final MostSelledModel mostSelledModel = mostSelledModels.get(i);

        try {
            Glide.with(context).load(mostSelledModel.getImage()).into(viewHolder.image);

            final GradientDrawable gradientDrawable = (GradientDrawable) viewHolder.product_basket_cicle_shape.getBackground();

            if(isColorWhite(mostSelledModel)){
                viewHolder.product_basket_cicle_shape.setBackground(context.getResources().getDrawable(R.drawable.imgcolorborder));
            }else
                gradientDrawable.setColor(Color.parseColor(mostSelledModel.getColor()));

            viewHolder.cmimi.setText(new DecimalFormat("##.##").format(mostSelledModel.getPrice()) + " €");

            viewHolder.madhesia.setText(mostSelledModel.getSize());
            viewHolder.sasia.setText("x" + mostSelledModel.getQuantity());
            viewHolder.klienti.setText(mostSelledModel.getClient());
            viewHolder.posta.setText(mostSelledModel.getShipper());
            viewHolder.sales.setText(mostSelledModel.getSales() + " shitje");
        }catch (Exception e){

        }
    }

    private boolean isColorWhite(MostSelledModel mostSelledModel) {
        String[] arr = new String[] {"#FFFFFF", "#FFFFF", "#FFFF", "#FFF", "#FF","#F", "#ffffff","#fffff","#ffff","#fff","#ff","#f"};

        for(int i = 0 ; i < arr.length ; i++){
            if(mostSelledModel.getColor().equals(arr[i]))
                return true;
        }

        return false;
    }

    @Override
    public int getItemCount() {
        return mostSelledModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image,product_basket_cicle_shape;
        TextView madhesia,sasia,klienti,posta,cmimi,sales;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            product_basket_cicle_shape = itemView.findViewById(R.id.product_basket_cicle_shape);
            madhesia = itemView.findViewById(R.id.madhesia);
            klienti = itemView.findViewById(R.id.klienti);
            posta = itemView.findViewById(R.id.posta);
            cmimi = itemView.findViewById(R.id.cmimi);
            sales = itemView.findViewById(R.id.sales);
        }
    }
}
