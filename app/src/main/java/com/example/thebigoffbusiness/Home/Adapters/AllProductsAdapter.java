package com.example.thebigoffbusiness.Home.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.thebigoffbusiness.Home.Model.SelectedDayProductsModel;
import com.example.thebigoffbusiness.R;

import java.text.DecimalFormat;
import java.util.List;

public class AllProductsAdapter extends RecyclerView.Adapter<AllProductsAdapter.ViewHolder> {

    private Context context;
    private List<SelectedDayProductsModel> selectedDayProductsModelList;

    private LinearLayoutManager linearLayoutManager;
    private SingleProductsAdapter singleProductsAdapter;

    public AllProductsAdapter(Context context, List<SelectedDayProductsModel> selectedDayProductsModelList) {
        this.context = context;
        this.selectedDayProductsModelList = selectedDayProductsModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.all_products_item_layout, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final SelectedDayProductsModel selectedDayProductsModel = selectedDayProductsModelList.get(i);

        try {
            viewHolder.txt_day.setText(selectedDayProductsModel.getDay());
            viewHolder.totali.setText(new DecimalFormat("##.##").format(selectedDayProductsModel.getTotal()) + " €");

            linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            singleProductsAdapter = new SingleProductsAdapter(context, selectedDayProductsModel.getSingleProductsModelList());

            viewHolder.recyclerView.setLayoutManager(linearLayoutManager);
            viewHolder.recyclerView.setAdapter(singleProductsAdapter);
        }catch (Exception e){

        }
    }

    @Override
    public int getItemCount() {
        return selectedDayProductsModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_day,totali;
        private RecyclerView recyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_day = itemView.findViewById(R.id.txt_day);
            totali = itemView.findViewById(R.id.totali);
            recyclerView = itemView.findViewById(R.id.recyclerview);
        }
    }
}
