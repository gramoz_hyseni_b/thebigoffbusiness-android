package com.example.thebigoffbusiness.Notification;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.viewpager.widget.ViewPager;

import com.example.thebigoffbusiness.Home.Adapters.TabViewPagerAdapter;
import com.example.thebigoffbusiness.Notification.Fragments.ConfirmOrdersFragment;
import com.example.thebigoffbusiness.Notification.Fragments.NotificationFragment;
import com.example.thebigoffbusiness.R;
import com.google.android.material.tabs.TabLayout;

public class NotificationActivity extends AppCompatActivity {

    private TabLayout tabs;
    private ViewPager viewpager;
    private ImageView profile;
    private TabViewPagerAdapter pagerAdapter;

    private ConfirmOrdersFragment confirmOrdersFragment;
    private NotificationFragment notificationFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        tabs = findViewById(R.id.tabs);
        viewpager = findViewById(R.id.viewpager);
        profile = findViewById(R.id.profile);

        pagerAdapter = new TabViewPagerAdapter(getSupportFragmentManager());

        notificationFragment = new NotificationFragment();
        confirmOrdersFragment = new ConfirmOrdersFragment();

        pagerAdapter.addFragment(notificationFragment, "Njoftimet");
        pagerAdapter.addFragment(confirmOrdersFragment,  "Konfirmo Porositë");

        tabs.setupWithViewPager(viewpager);
        viewpager.setAdapter(pagerAdapter);
    }
}
