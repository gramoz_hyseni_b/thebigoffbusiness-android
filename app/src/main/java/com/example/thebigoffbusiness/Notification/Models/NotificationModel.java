package com.example.thebigoffbusiness.Notification.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dell on 1/25/2018.
 */

public class NotificationModel {
    @SerializedName("NotificationID")
    @Expose
    private int NotificationID;
    @SerializedName("Icon")
    @Expose
    private String Icon;
    @SerializedName("Title")
    @Expose
    private String Title;
    @SerializedName("Body")
    @Expose
    private String Body;
    @SerializedName("Seen")
    @Expose
    private Boolean Seen;
    @SerializedName("Date")
    @Expose
    private String Date;
    @SerializedName("Meta_key")
    @Expose
    private String MetaKey;
    @SerializedName("Meta_value")
    @Expose
    private String MetaValue;

    public int getNotificationID() {
        return NotificationID;
    }

    public void setNotificationID(int notificationID) {
        NotificationID = notificationID;
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getBody() {
        return Body;
    }

    public void setBody(String body) {
        Body = body;
    }

    public Boolean getSeen() {
        return Seen;
    }

    public void setSeen(Boolean seen) {
        Seen = seen;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getMetaKey() {
        return MetaKey;
    }

    public void setMetaKey(String metaKey) {
        MetaKey = metaKey;
    }

    public String getMetaValue() {
        return MetaValue;
    }

    public void setMetaValue(String metaValue) {
        MetaValue = metaValue;
    }

    public NotificationModel(int notificationID, String icon, String title, String body, Boolean seen, String date, String metaKey, String metaValue) {
        NotificationID = notificationID;
        Icon = icon;
        Title = title;
        Body = body;
        Seen = seen;
        Date = date;
        MetaKey = metaKey;
        MetaValue = metaValue;
    }
}
