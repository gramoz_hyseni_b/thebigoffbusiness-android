package com.example.thebigoffbusiness.Notification.Fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.thebigoffbusiness.Notification.Adapters.NotificationAdapter;
import com.example.thebigoffbusiness.Notification.Models.NotificationModel;
import com.example.thebigoffbusiness.R;

import java.util.ArrayList;

public class NotificationFragment extends Fragment {

    private RecyclerView recyclerview;
    private NotificationAdapter notificationAdapter;
    private LinearLayoutManager linearLayoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View roortView = inflater.inflate(R.layout.notification_fragment_layout, container, false);

        recyclerview = roortView.findViewById(R.id.recyclerview);

        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        notificationAdapter = new NotificationAdapter(getContext(), new ArrayList<NotificationModel>());

        recyclerview.setLayoutManager(linearLayoutManager);
        recyclerview.setAdapter(notificationAdapter);

        return roortView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
