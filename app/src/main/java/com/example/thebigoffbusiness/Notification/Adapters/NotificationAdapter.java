package com.example.thebigoffbusiness.Notification.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.thebigoffbusiness.Notification.Models.NotificationModel;
import com.example.thebigoffbusiness.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private Context context;
    private List<NotificationModel> notificationModelList;
    private AlertDialog.Builder builder;

    public NotificationAdapter(Context context, List<NotificationModel> notificationModelList) {
        this.context = context;
        this.notificationModelList = notificationModelList;

        notificationModelList.add(new NotificationModel(1,"https://thebigoff.com/Content/profilePicture191614336.png","Title","Body",false,new Date().toString(),"ProductID","1"));
        notificationModelList.add(new NotificationModel(1,"https://thebigoff.com/Content/profilePicture191614336.png","Title","Body",true,new Date().toString(),"ProductID","1"));
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_item_model, viewGroup, false);

        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        final NotificationModel model = notificationModelList.get(i);

        if(model.getSeen()){
            Drawable background = context.getResources().getDrawable( R.drawable.item_ripple_custom );
            int color = Color.GRAY;
            viewHolder.rel.setBackground(getBackgroundDrawable(color, background));
        }else if(!model.getSeen()){
            Drawable background = context.getResources().getDrawable( R.drawable.item_ripple_custom_2 );
            int color = Color.GRAY;
            viewHolder.rel.setBackground(getBackgroundDrawable(color, background));
        }

        try
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss");
//            Date past = format.parse( getSplited(model.getDate())+ " at 23:59:30");
            Date past = format.parse( getSplited(model.getDate()) + " AD at " + model.getDate().substring(11,19));

            Date now = new Date();
            long seconds= TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes=TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours=TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days=TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());

            if(seconds<60)
                viewHolder.date.setText(seconds+ " sekonda më parë");
            else if(minutes<60)
                viewHolder.date.setText(minutes+ " minuta më parë");
            else if(hours<24)
                viewHolder.date.setText(hours+ " orë më parë");
            else
                viewHolder.date.setText(days+ " ditë më parë");
        }
        catch (Exception j){
            //viewHolder.date.setText(model.getDate().substring(0,10));
            viewHolder.date.setText(getSplited(model.getDate()));
            j.printStackTrace();
        }

        try {
            if(!model.getIcon().equals(""))
                Glide.with(context)
                        .load(model.getIcon())
                        .apply(new RequestOptions().circleCrop().placeholder(R.drawable.no_loading_photo))
                        .into(viewHolder.icon);
            else
                Glide.with(context)
                        .load(R.mipmap.ic_launcher_round)
                        .apply(new RequestOptions().circleCrop())
                        .into(viewHolder.icon);

        }catch (Exception e){
            e.printStackTrace();
        }
        viewHolder.title.setText(model.getTitle()+"");
        if(model.getBody() == null || model.getBody().equals("")) {
            viewHolder.body.setVisibility(View.GONE);
        }else
            viewHolder.body.setText(model.getBody() + "");

        viewHolder.notification_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.setMessage("Dëshironi të fshini këtë njoftim?");

                builder.setPositiveButton("PO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        removeNotification(i, model.getNotificationID());
                    }
                });

                builder.setNegativeButton("JO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

                //removeNotification(i, model.getNotificationID());
            }
        });
    }

    private void removeNotification(int position, int notificationID){

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static RippleDrawable getBackgroundDrawable(int pressedColor, Drawable backgroundDrawable)
    {
        return new RippleDrawable(getPressedState(pressedColor), backgroundDrawable, null);
    }

    public static ColorStateList getPressedState(int pressedColor)
    {
        return new ColorStateList(new int[][]{ new int[]{}},new int[]{pressedColor});
    }
    private String getSplited(String date) {
        String[] arr = date.substring(0,10).split("-");

        String txt = "";

        for(int i = 0 ; i < arr.length ; i++){
            txt+= arr[i] + ".";
        }

        return txt.substring(0, txt.length()-1);
    }

    @Override
    public int getItemCount() {
        return notificationModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title,body,date;
        ImageView icon;
        LinearLayout notification_remove;
        RelativeLayout rel;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.notification_title);
            body = itemView.findViewById(R.id.notification_description);
            icon = itemView.findViewById(R.id.notification_image_id);
            notification_remove = itemView.findViewById(R.id.notification_remove);
            rel = itemView.findViewById(R.id.rel);
            date = itemView.findViewById(R.id.date);
        }
    }
}
